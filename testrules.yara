rule testrule
{
meta:
    description = "Test YARA rule"

strings:
    $a = "Example string"
    $b = "Oh no, virus!"
    $syntax1 = "<<"

condition:
    ($syntax1 and $a or $b)
}
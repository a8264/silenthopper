# SILENTHOPPER v1.0.1 - WRITTEN BY BLUNT MOAK FROG
# AMPHIBIAN SKIN ANTI-FUCK SOLUTIONS 2022
# QUALITY SPAGHETTIWARE <3

import time
import os
import argparse
import yara
import datetime
import sys
from pynput import keyboard
from termcolor import colored, cprint

# Initialize main variables to keep track of them, just makes it easy for me
class initVars():
  global formatResults
  global writeResults
  global results
  inDir = True
  malwareArg = True
  wait = True
  if os.name == "nt":
    dirDivide = "\\"
  elif os.name == "posix":
    dirDivide = "/"
  sysPlatform = sys.platform()
  date = datetime.datetime.now()
  fileName = "silenthopper-scan-{}-{}-{}-{}.{}.{}.txt".format(date.year, date.month, date.day, date.hour, date.minute, date.second)
  writeDate = "SilentHopper Scan from: {}-{}-{} {}:{}:{}\n".format(date.year, date.month, date.day, date.hour, date.minute, date.second)

# Initializing variables for directory/system scans
class dirVars():
  currentDir = ""
  dirName = ""
  freeBSDdirs = ["/bin/", "/boot/", "/dev/", "/etc/", "/mnt/", "/proc/", "/rescue/", "/root/", "/sbin/", "/tmp/", "/usr/", "/var/"]
  linuxDirs = ["/bin/", "/boot/", "/dev/", "/etc/", "/home/", "/lib/", "/media/", "/mnt/", "/opt/", "/proc/", "/run/", "/sbin/", "/srv/", "/tmp/", "/usr/", "/var/"]
  macOSdirs = ["/Applications/", "/bin/", "/cores/", "/etc/", "/home/", "/Library/", "/net/", "/Network/", "/private/", "/sbin/", "/System/", "/tmp/", "Users", "/usr/", "/var/"]
  openBSDdirs = ["/altroot/", "/bin/", "/boot/", "/bsd/", "/dev/", "/etc/", "/home/", "/mnt/", "/root/", "/sbin/", "/sys/", "/tmp/", "/usr/", "/var/"]
  rootDir = ""
  subscanDir = ""
  tempDir = ""
  useUnixDirs = True

# Initializing variables for file scans
class fileVars():
  global fileInput
  global logFile
  currentFile = ""
  fileExt = [".yara", ".txt", ".md"]
  flaggedFiles = []
  fileOutput = ""
  malware = ""
  tempFile = ""
  writeData = ""

# Parse shell args
class userArgs():
  parser = argparse.ArgumentParser(prog="SilentHopper", description="SilentHopper is a simple, cross-platform, and open source Antivirus using Python3 and YARA that runs in the system shell. Written by Blunt Moak Frog of Amphibian Skin Anti-Fuck Solutions 2022.", add_help=False)
  parser.set_defaults(performScan=False)
  parser.set_defaults(performDirScan=False)
  parser.set_defaults(performSysScan=False)
  parser.add_argument("-h", "--help", action='help', help="Brings up the SilentHopper help menu.")
  parser.add_argument("-v", "--version", action = "version", version='''%(prog)s v1.0.1 - Created by Blunt Moak Frog of Amphibian Skin Anti-Fuck Solutions - "I'm fucked and idk what to do"''', help="Shows the currently installed version of %(prog)s.")
  parser.add_argument("-s", "--scan", dest='performScan', action="store_true", help="Scans a single file based on the provided YARA rules within the source code.")
  parser.add_argument("-ds", "--dirscan", dest='performDirScan', action="store_true", help="Scans a listed directory's contents and matches the listed files to the provided YARA rules.")
  parser.add_argument("-ss", "--sysscan", dest='performSysScan', action="store_true", help="Scans all files and directories on the system hard drive. WARNING: Can take quite long. Must be ran with administrator privileges. (On Windows: Run PowerShell as Administator | On Linux/macOS: Run with sudo or as Root | On BSD: Run with doas or as Root")
  args = parser.parse_args()

# Program start, also some vars for YARA and initializes MOAR vars
class yaraVars():
  virusFile = ""
  virusDir = []
  def programStart():
    time.sleep(1)
    cprint('''
          _____ _ __           __  __  __                           
         / ___/(_) /__  ____  / /_/ / / /___  ____  ____  ___  _____
         \__ \/ / / _ \/ __ \/ __/ /_/ / __ \/ __ \/ __ \/ _ \/ ___/
        ___/ / / /  __/ / / / /_/ __  / /_/ / /_/ / /_/ /  __/ /    
       /____/_/_/\___/_/ /_/\__/_/ /_/\____/ .___/ .___/\___/_/     
                                          /_/   /_/                
    ''', 'green')
    time.sleep(1)
    cprint ('''
            #    .##         ###OOOOOOOOOOOOOOOO###.        ###    o#      
           # #OOOOOOOOO###OOOOOOOOOO########OOOOOOOOOO###OOOOOOOoO#        
          # #OO#####OOoOOOOO#########################OOOOooO#####OoO###    
         # #OO###OOoooOO################################OOOooOO###Oo# #    
         # #Oo##OooOO######################################OOoOOO##o# #    
          # #OooOoO##########################################OoOOooO###    
           # ##Oo##############################################oO##        
           # #Oo##O###################OOOO######################oO# #      
          # #Oo##OoooOOO##OOOOoooo****°°°°°****oooOOOOO###OOOoO##oO# #     
         # #Oo#######OooOoOo°..                  ..°°oOOOooOOO####oO# #    
        # #Oo#######OOo°....°°°°.................°°°°°°.°*OO#######oO# #   
       ###Oo#######oOo°°°°°°..°.................°°°......°oOo#######oO# #  
    ######oO#######oO ....*ooOo.                 °°°°°°ooo*Ooo######Oo# #  
   #     ##oOO#####oo*..    ...°°***oooooooo***°°..     ..°ooO####OOo####  
    #####  #OOOO####OOOo*****oOOO##############OOOoo****oOOO####OOoO#      
 ###O*Oo###  ##OOOO###OoooOO########################OOOoOO###OOOOO#  ##    
 ###OoOoooOO#.  #OOOOoooO##############################OOooOOOO##  o#      
     ##OOoo*oO##  ##OOoO################################OoOO##   O#        
   #   ##OOo*ooO##  .##OOOOO########################OOOOO##   o##          
     ##  ##OoOoo*O#OoOOOOOooooOOO###############OOooooOOOO## ##            
       ##   ##OOoOOoooOO#####OOooooOOOO##OOOOoooooOO####OOoO#  O#          
         ##O O#OOOOoOooOoooOOO####OOOOOooooOOO####OOOoooOooOO##  ##        
            #O##OooO#oOoOOOOoooooOO####Oo*O###OooooooooooOoOOOOO#  #       
           o .OoOO##oOo########OOooooO###OOooooOOOOooO###oOo###OoO#  #     
         #  #OOO###oOoO#############OooooooO###OOooO######oOo####Oo#. #    
        # .#oO####OooO#################OO###OoooOO########OoOo####OoO# #   
       #O#OoO#####oOo####################OoooOO############OooO#####OO#°#  
       # #ooO####oOo#################OOoooOO######OOO#OOO###oOo####OOo#.   
      ##Oo..°*oOOooO#############OOooooOO#######O#o.°#°.*####oOoOOo*°.*O###
     ###o*O°...°oOo##########OOooooOO##########*.°Oo°O.Oo..O#oOo°.°°.Ooo# #
         .#*o#°°OoO####OOOooooOOO###############OOo°...°ooo###oO*°##*O     
       .   # #o##*OOooooOOO########################*...o#####OoO#oO   °*#  
          . *  #oOOo*ooOOO##########################OOOOOOooooOoO. # #     
           ####Oo###OOOOOoooooooOooOOooOOOooOooOoooooooOOOOO###Oo# #       
            # #o################OOO#OOooOoOOOOOO################oO###      
           # #Oo######################OoOo######################oO# #      
           # #oO######################ooOo#######################o# #      
           # #OoOO####################oo#*####################OOOo# # #    
              ##OOOOOOOOOOOOOOOOOOOOOOoO#oOOOOOOOOOOOOOOOOOOOOOOO#         
             #       .##OOOOOOOOOOOOOO####OOOOOOOOOOOOO####       #  ###   
    ''', "green")
    time.sleep(1)
    cprint("[ WRITTEN BY BLUNT MOAK FROG OF AMPHIBIAN SKIN ANTI-FUCK SOLUTIONS 2022 ]", "green")
  programStart()
  yaraRules = yara.compile(filepath='testrules.yara')
  if userArgs.args.performScan == True: # Single File Scan
    fileVars.fileInput = input("Input the path to your file: ")
    virusFile = fileVars.fileInput
  elif userArgs.args.performDirScan == True: # Directory Scan
    fileVars.fileInput = input("Input the path to your directory: ")
    virusDir = fileVars.fileInput
    userArgs.args.performScan = True
  elif userArgs.args.performSysScan == True: # System Scan
    userArgs.args.performScan = True
    if os.name == "nt":
      dirVars.useUnixDirs = False
      yaraVars.virusFile = "C:\\"
    elif os.name == "posix":
      dirVars.useUnixDirs = True
      if sys.platform() == 'darwin':
        yaraVars.virusDir = dirVars.macOSdirs
      elif sys.platform() == 'freebsd':
        yaraVars.virusDir = dirVars.freeBSDdirs
      elif sys.platform() == 'linux':
        yaraVars.virusDir = dirVars.linuxDirs
      elif sys.platform() == 'openbsd5' or 'netbsd':
        yaraVars.virusDir = dirVars.openBSDdirs

# Checks if the inputted file happens to be a blacklisted extension
def extCheck():
  for x in fileVars.fileExt:
    if fileVars.malware.endswith(x):
      return False
      break
  return True

# YARA callback function
def callbackFunc(data):
  if extCheck() == True:
    cprint("YARA match detected!", 'red')
    fileVars.flaggedFiles.append(fileVars.malware)
    time.sleep(0.5)
    cprint("Logging YARA match information on sample...", 'yellow')
    time.sleep(0.5)
    initVars.results = data
    initVars.formatResults = str(data)
    initVars.writeResults = initVars.formatResults + "\n"
    fileVars.logFile = open(initVars.fileName, "a")
    fileVars.logFile.write(initVars.writeResults)
    fileVars.logFile.close()
  else:
    pass
  return yara.CALLBACK_CONTINUE

# On Press for Keyboard Listener
def on_press(key):
  if key == keyboard.Key.enter:
    fileVars.malwareArg = True
    initVars.wait = False
    malwareParse(fileVars.flaggedFiles, fileVars.malwareArg)
  elif key == keyboard.Key.backspace:
    fileVars.malwareArg = False
    initVars.wait = False
    malwareParse(fileVars.flaggedFiles, fileVars.malwareArg)

# Listener for malware args
def malwareFound():
  time.sleep(1)
  cprint("Press Enter for Yes or press Backspace for No", "yellow")
  with keyboard.Listener(on_press=on_press) as listener:
    while initVars.wait == True:
      time.sleep(0.5)
    listener.join()

# Parses args from malwareFound()
def malwareParse(a, b):
  if b == True:
    cprint("Removing Malware...", "yellow")
    time.sleep(1)
    for x in a:
      fileVars.writeData = "Detected and deleted malware found in: " + x
      fileVars.logFile = open(initVars.fileName, "a")
      fileVars.logFile.write(fileVars.writeData)
      fileVars.logFile.close()
      os.remove(x)
      cprint("Removed " + x)
      time.sleep(0.5)
    cprint("Malware removed!", "green")
    time.sleep(1)
    os._exit(0)
  else:
    print("Exiting SilentHopper...")
    time.sleep(1)
    os._exit(0)

# The file scan with YARA itself
def matchRules(a):
  fileVars.malware = a
  matches = yaraVars.yaraRules.match(a, callback=callbackFunc, which_callbacks=yara.CALLBACK_MATCHES, timeout=120)
  time.sleep(1)

# Scans a directory fed into the function
def dirScan(a):
  dirVars.tempDir = os.listdir(a)
  for x in dirVars.tempDir:
    try:
      fileVars.tempFile = a + x
      cprint("Scanning " + fileVars.tempFile, 'yellow')
      matchRules(fileVars.tempFile)
    except:
      dirVars.subscanDir = a + x + initVars.dirDivide
      cprint("Scanning " + dirVars.subscanDir, 'yellow')
      dirScan(dirVars.subscanDir)

# All the other gibberish that ACTUALLY does the thing
if userArgs.args.performScan == True:
  fileVars.logFile = open(initVars.fileName, "x")
  fileVars.logFile.write(initVars.writeDate)
  fileVars.logFile.close()
  time.sleep(1)  
  cprint("Starting scan...", "yellow")
  if userArgs.args.performSysScan == True:
    if dirVars.useUnixDirs == True:
      dirVars.currentDir = yaraVars.virusDir
      dirVars.currentDir = os.listdir()
      for x in dirVars.currentDir:
        cprint("Scanning " + x, 'yellow')
        dirScan(x)
    else:
      dirVars.currentDir = os.listdir(yaraVars.virusFile)
      for x in dirVars.currentDir:
        cprint("Scanning " + x, 'yellow')
        dirScan(x)
  elif userArgs.args.performDirScan == True:
    cprint("Scanning " + yaraVars.virusDir, 'yellow')
    dirVars.currentDir = os.listdir(yaraVars.virusDir)
    for x in dirVars.currentDir:
      try:
        fileVars.currentFile = yaraVars.virusDir + x
        cprint("Scanning " + fileVars.currentFile, 'yellow')
        matchRules(fileVars.currentFile)
      except:
        dirVars.currentDir = yaraVars.virusDir + x + initVars.dirDivide
        dirScan(dirVars.currentDir)
  else:
    fileVars.currentFile = yaraVars.virusFile
    cprint("Scanning " + fileVars.currentFile, 'yellow')
    matchRules(fileVars.currentFile)
  try:
    initVars.results
  except:
    cprint("No malware found!", "green")
    os._exit(0)
  else:
    time.sleep(1)
    for x in fileVars.flaggedFiles:
      cprint("Malicious string found in: " + x, 'red')
      time.sleep(0.5)
    cprint("Malware was found and logged! Would you like to delete it?", "red")
    malwareFound()
else:
  os._exit(0)
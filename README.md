# SILENTHOPPER v1.0.1 ALPHA - THE OPEN-SOURCE CROSS-PLATFORM ANTIVIRUS

![SilentHopper Banner](silenthopperbanner.png)

SilentHopper is an open-source cross-platform anti-malware scan made for hobby purposes. Currently, the YARA rules do not actually detect real malware in the wild, but it can be configured to use a set of YARA rules as you desire.

### UPDATES:
- Directory scanning is now fully functional
- Linux System scan functional, Windows System scan not yet tested

### HOW TO SETUP:
##### Linux & MacOS:
- Clone the .git repository in your Terminal and navigate into the directory
- Use the following command to ensure you have all the necessary Python3 modules:<br>
```bash
pip3 install requirements.txt
```
- To run SilentHopper, use following syntax in Terminal:
```bash
python3 silenthopper.py <argument>
```

- To properly perform system scans, use sudo to ensure that you can access directories and files properly
- To run SilentHopper, use following syntax in Terminal:
```bash
sudo python3 silenthopper.py <argument>
```

##### FreeBSD, OpenBSD, & NetBSD:
- Clone the .git repository in your Terminal and navigate into the directory
- Read the modules in the requirements.txt and use this command to ensure you have all the necessary Python3 modules:<br>
```bash
python3 -m pip install <module>
```
- To run SilentHopper, use following syntax in Terminal:
```bash
python3 silenthopper.py <argument>
```

- To properly perform system scans, use doas to ensure that you can access directories and files properly
- To run SilentHopper, use following syntax in Terminal:
```bash
doas python3 silenthopper.py <argument>
```

- WARNING: OpenBSD support is not fully functional yet, as there are still tests being done regarding the yara-python4.1.0 module and OpenBSD

##### Windows:
- Download and install the most recent version of Python3 for your version of Windows if you already haven't
- Edit your Environment Variables to feature the path to the Python IDLE
- Open your Windows PowerShell, navigate to where you downloaded SilentHopper in PowerShell, and install the requirements:
```powershell
python -m pip install requirements.txt
```
- Once finished installing, to run SilentHopper simply use the following syntax in PowerShell:
```powershell
python silenthopper.py [argument]
```
- In order to perform system scans, run Windows PowerShell as Administrator

### ARGUMENTS:
- -h, --help      Brings up the SilentHopper help menu.
- -v, --version   Shows the currently installed version of SilentHopper.
- -s, --scan      Scans a single file based on the provided YARA rules within the source code.
- -ds, --dirscan  Scans a listed directory's contents and matches the listed files to the provided YARA rules.
- -ss, --sysscan  Scans all files and directories on the system hard drive. WARNING: Can take quite long.
